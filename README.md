## Please use this issue tracker to discuss Codeberg-related issues.

### Link to the issue tracker is [here](https://codeberg.org/Codeberg/Community/issues).

## Official Codeberg documentation is on [docs.codeberg.org](https://docs.codeberg.org)

### If you like to share your insights and experiences and add to our documentation, please consider contributing to the [documentation repository](https://codeberg.org/Codeberg/Documentation).

